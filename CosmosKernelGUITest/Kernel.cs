﻿using System;
using static System.Drawing.Color;
using System.Collections.Generic;
using System.Text;
using Sys = Cosmos.System;
using CGS = Cosmos.System.Graphics;

namespace CosmosKernelGUITest
{
    public class Kernel : Sys.Kernel
    {

        private CGS.Canvas canvas;
        private float buttonSize = 0f;
        private GUI.Button[] buttonArray;
        private readonly int num_buttons = 7;

        public static System.Drawing.Color[] buffer;
        public static System.Drawing.Color[] buffer_old;
        public static int screenX;
        public static int screenY;

        protected override void BeforeRun()
        {
            Console.WriteLine("Booted successfully. Going to graphics mode.");
            this.canvas = CGS.FullScreenCanvas.GetFullScreenCanvas();
            //this.canvas.Clear(DarkOrange);
            //var pen = new CGS.Pen(Black);
            screenY = canvas.Mode.Rows;
            screenX = canvas.Mode.Columns;
            this.canvas.Mode = new CGS.Mode(screenX, screenY, CGS.ColorDepth.ColorDepth32);

            //Initialising buffers
            buffer = new System.Drawing.Color[(screenX * screenY) + screenX];
            buffer_old = new System.Drawing.Color[(screenX * screenY) + screenX];


            //canvas.DrawFilledRectangle(pen, 0, 0, screenX / 8, screenY);
            buttonSize = screenX / 12;
            this.buttonArray = new GUI.Button[this.num_buttons];
            for(int i = 0; i < this.num_buttons; i++)
            {
                buttonArray[i] = new GUI.Button((int) buttonSize, i, true);
            }
            Sys.MouseManager.ScreenHeight = Convert.ToUInt32(screenY);
            Sys.MouseManager.ScreenWidth = Convert.ToUInt32(screenX);
            BasicBufferFill();
            DrawScreen();
        }

        protected override void Run()
        {
            BasicBufferFill();
            DrawScreen();
            if(Sys.MouseManager.MouseState == Sys.MouseState.Left)
            {
                var x = Sys.MouseManager.X;
                var y = Sys.MouseManager.Y;
                var x_pos = Convert.ToInt32(x);
                var y_pos = Convert.ToInt32(y);
                for (int i = 0; i < 7; i++)
                {
                    if(this.buttonArray[i].Is_In(x_pos, y_pos))
                    {
                        this.buttonArray[i].Toggle();
                        //break;
                    }
                }
            }
        }

        public void SetPixel(int x, int y, System.Drawing.Color c)
        {
            Kernel.buffer[(y * screenX) + x] = c;
        }

        protected void DrawScreen()
        {
            var pen = new CGS.Pen(Orange);
            for(int y = 0; y < screenY; y++)
            {
                for(int x = 0; x < screenX; x++)
                {
                    int pos = (y * screenX) + x;
                    if (Kernel.buffer[pos] != Kernel.buffer_old[pos])
                    {
                        pen.Color = Kernel.buffer[pos];
                        this.canvas.DrawPoint(pen, x, y);
                    }
                    Kernel.buffer_old[pos] = Kernel.buffer[pos];
                }
            }
        }

        protected void ClearScreen(System.Drawing.Color c)
        {
            for(int i = 0; i < Kernel.buffer.Length; i++)
            {
                Kernel.buffer[i] = c;
            }
        }

        protected void DrawBackground()
        {
            for(int x = 0; x < screenX; x++)
            {
                for(int y = 0; y < screenY; y++)
                {
                    if (x < screenX / 8)
                    {
                        this.SetPixel(x, y, Black);
                    }
                    else
                    {
                        this.SetPixel(x, y, DarkOrange);
                    }
                }
            }
        }

        protected void DrawMouseCursor()
        {
            uint x = 50;
            uint y = 50;
            if (!(Sys.MouseManager.X == Convert.ToUInt32(null)))
            {
                x = Sys.MouseManager.X;
                y = Sys.MouseManager.Y;
            }
            var x_pos = Convert.ToInt32(x);
            var y_pos = Convert.ToInt32(y);
            int size = 6;
            for(int x_pix = x_pos - size; x_pix < x_pos + size; x_pix++)
            {
                for(int y_pix = y_pos - size; y_pix < y_pos + size; y_pix++)
                {
                    this.SetPixel(x_pix, y_pix, Green);
                }
            }
        }

        protected void BasicBufferFill()
        {
            //Background
            DrawBackground();
            //Buttons
            for (int i = 0; i < this.num_buttons; i++)
            {
                buttonArray[i].AddToBuffer();
            }
            //Cursor
            DrawMouseCursor();
        }

    }
}

namespace GUI
{
    public class Button
    {

        private int _size;
        private int _position;
        private bool _enabled;
        private int _offset = 15;

        public Button(int size, int position, bool start_enabled)
        {
            this._size = size;
            this._position = position;
            this._enabled = start_enabled;
        }

        public void AddToBuffer()
        {
            int start_y = 10 + (this._position * (this._offset + this._size));
            var c = LightGray;
            if(!this._enabled)
            {
                c = YellowGreen;
            }
            for (int y = start_y; y < start_y + this._size; y++) 
            {
                for(int x = 15; x < 15 + this._size; x++)
                {
                    CosmosKernelGUITest.Kernel.buffer[(y * CosmosKernelGUITest.Kernel.screenX) + x] = c;
                }
            }
        }

        public bool Is_In(int x, int y)
        {
            bool in_x = x > 15 & x < (15 + this._size);
            bool in_y = y > 10 + (this._position * (this._offset + this._size)) & y < ((this._position + 1) * (this._offset + this._size));
            return in_x & in_y;
        }
        public void Toggle()
        {
            this._enabled = !this._enabled;
        }
    }
}
